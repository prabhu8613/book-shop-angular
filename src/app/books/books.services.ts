import { Injectable, EventEmitter } from "@angular/core";
import { Books } from "./Books.model";
import { CartServices } from "../shopping/cart.services";


@Injectable()
export class BookServices {
    constructor(private _cartServices: CartServices) { }
    selectedBook: Books;
    books: Books[] = [
        new Books("Test Book 1",
            "Prabhu 1",
            "https://about.canva.com/wp-content/uploads/sites/3/2015/01/business_bookcover.png",
            100),
        new Books("Test Book 2",
            "Prabhu",
            "https://www.bookish.com/wp-content/uploads/the-night-gardener-9781481439787_hr.jpg",
            150)
    ];

    createBook(book: Books) {
        this.books.push(book);
    }

    removeBook(id) {
        this.books.splice(id, 1);
    }

    setSelectedBook(index: number) {
        this.selectedBook = this.books[index];
    }


    addBookToCart(index: number) {
        this._cartServices.addBookToCart(this.books[index]);
    }

    getBook(itemIndex: number): Books {
        return this.books[itemIndex];
    }

    addToBookList(newEntry: Books) {
        if (this.isBookExist(newEntry)) {
            this.books[this.getBookIndex(newEntry)] = newEntry;
        } else {
            this.books.push(newEntry);
        }
    }

    private getBookIndex(newEntry: Books) {
        return this.books.findIndex(book => book.title === newEntry.title);
    }

    private isBookExist(newEntry: Books) {
        return this.books.findIndex(book => book.title === newEntry.title) > -1;
    }
}