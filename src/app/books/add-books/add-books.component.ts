import { Component, OnInit } from '@angular/core';
import { Books } from '../Books.model';
import { BookServices } from '../books.services';

@Component({
  selector: 'app-add-books',
  templateUrl: './add-books.component.html',
  styleUrls: ['./add-books.component.css']
})
export class AddBooksComponent implements OnInit {
  book: Books = new Books("", "", "", null);
  constructor(private _bookService: BookServices) {
  }

  ngOnInit() {
    // if (this.params !== undefined) {
    //   this.activatedRoute.params.subscribe((params) => {
    //     this.book = this._bookService.getBook(this.params.id);
    //   });
    // }
  }

  onSubmitAddComponent() {
    this._bookService.addToBookList(this.book);
  }
}
