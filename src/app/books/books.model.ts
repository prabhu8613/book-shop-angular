export class Books {

    constructor(public title: String,
                public author: String,
                public imagePath: String,
            public price: number) {}
}