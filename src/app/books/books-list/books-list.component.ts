import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Books } from '../Books.model';
import { BookServices } from '../books.services';
import { Router } from '@angular/router';
@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.css']
})
export class BooksListComponent implements OnInit {
  books: Books[];
  constructor(private _bookService: BookServices, private router: Router) { }

  onBookSelected(index: number) {
    this.router.navigate(['/View',index]);
  }

  ngOnInit() {
    this.books = this._bookService.books;
  }

  onClickAddToCart(index: number) {
    this._bookService.addBookToCart(index);
  }
}
