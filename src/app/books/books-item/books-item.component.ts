import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { Books } from '../Books.model';
import { BookServices } from '../books.services';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-books-item',
  templateUrl: './books-item.component.html',
  styleUrls: ['./books-item.component.css']
})
export class BooksItemComponent implements OnInit {
  selectedBook: Books;
  id: number;
  ngOnInit(): void {
    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params.id;
      this.selectedBook = this._bookService.getBook(this.id);
    });
  }
  onClickAddToCart() {
    this._bookService.addBookToCart(this.id);
  }
  constructor(private activatedRoute: ActivatedRoute, private _bookService: BookServices) {
  }
}
