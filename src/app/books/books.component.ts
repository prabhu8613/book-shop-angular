import { Component, OnInit } from '@angular/core';
import { Books } from './Books.model';
import { BookServices } from './books.services';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  constructor() { }
  ngOnInit() {
  }
}
