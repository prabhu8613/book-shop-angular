import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BooksComponent } from './books/books.component';
import { ShoppingComponent } from './shopping/shopping.component';
import { BooksItemComponent } from './books/books-item/books-item.component';
import { BooksListComponent } from './books/books-list/books-list.component';
import { AddBooksComponent } from './books/add-books/add-books.component';
import { BookServices } from './books/books.services';
import { CartServices } from './shopping/cart.services';


const appRoute: Routes = [{ path: "Books", component: BooksListComponent },
{ path: "Cart", component: ShoppingComponent },
{ path: "Book", component: AddBooksComponent },
{ path: "View/:id", component: BooksItemComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BooksComponent,
    ShoppingComponent,
    BooksItemComponent,
    BooksListComponent,
    AddBooksComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoute)
  ],
  providers: [BookServices, CartServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
