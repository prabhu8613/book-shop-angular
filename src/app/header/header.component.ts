import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { CartServices } from '../shopping/cart.services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  cartCount: number[];
  @Output() pageSelectedEvent = new EventEmitter();
  constructor(private _cartServices: CartServices) { }

  ngOnInit() {
    this.cartCount = this._cartServices.getCartCount();
    this._cartServices.cartCountEvent.subscribe(count => this.cartCount = count);
  }

  pageSelected(selectedPage: String) {
    this.pageSelectedEvent.emit(selectedPage);
  }

}
