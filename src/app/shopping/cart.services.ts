import { Injectable, EventEmitter } from "@angular/core";
import { Books } from "../books/Books.model";
import { Cart } from "./cart.model";

@Injectable()
export class CartServices {
    private cart: Cart[] = [];
    constructor() { }

    shoppingListEvent = new EventEmitter<Cart[]>();
    cartCountEvent = new EventEmitter<number[]>();
    getCartDetails() {
        return this.cart;
    }

    getCartCount() {
        var total = [0, 0];
        for (var i = 0, _len = this.cart.length; i < _len; i++) {
            total[0] += this.cart[i].quantity;
            total[1] += this.cart[i].book.price * this.cart[i].quantity;
        }
        return total
    }

    addBookToCart(book: Books) {
        let bookExistInCart = this.cart.find(x => x.book.title == book.title);
        if (bookExistInCart !== undefined) {
            this.cart[this.cart.indexOf(bookExistInCart)] = new Cart(bookExistInCart.book, bookExistInCart.quantity + 1);
        } else
            this.cart.push(new Cart(book, 1));
        this.shoppingListEvent.emit(this.cart);
        this.cartCountEvent.emit(this.getCartCount());
    }

    alterCartItem(isCartAddition, itemIndex) {
        if (isCartAddition === false)
            this.reduceQuantity(itemIndex);
        else
            this.increaseQuantity(itemIndex);
        this.cartCountEvent.emit(this.getCartCount());
    }

    private increaseQuantity(itemIndex: any) {
        this.cart[itemIndex].quantity++;
    }

    private reduceQuantity(itemIndex: any) {
        this.isLastItemInCart(itemIndex) ? this.removeFromCart(itemIndex) : this.cart[itemIndex].quantity--;
    }

    private isLastItemInCart(itemIndex: any) {
        return this.cart[itemIndex].quantity === 1;
    }

    RemoveItemFromCart(itemIndex: number) {
        this.removeFromCart(itemIndex);
        this.emitShoppingListEvent();
        this.emitCartTotalEvent();
    }

    private emitCartTotalEvent() {
        this.cartCountEvent.emit(this.getCartCount());
    }

    private emitShoppingListEvent() {
        this.shoppingListEvent.emit(this.cart);
    }

    private removeFromCart(itemIndex: any) {
        return this.cart.splice(itemIndex, 1);
    }
}