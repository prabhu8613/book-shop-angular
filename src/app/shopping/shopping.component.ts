import { Component, OnInit } from '@angular/core';
import { Cart } from './cart.model';
import { CartServices } from './cart.services';

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.component.html',
  styleUrls: ['./shopping.component.css']
})
export class ShoppingComponent implements OnInit {
  cartList: Cart[];
  constructor(private _cartService: CartServices) { }

  ngOnInit() {
    this.cartList = this._cartService.getCartDetails();
    this._cartService.shoppingListEvent.subscribe(newCart => this.cartList = newCart);
  }

  onRemoveItemToCart(itemIndex: number) {
    this._cartService.alterCartItem(false, itemIndex);
  }
  onAddItemToCart(itemIndex: number) {
    this._cartService.alterCartItem(true, itemIndex);
  }

  onRemoveItemFromCart(itemIndex: number) {
    this._cartService.RemoveItemFromCart(itemIndex);
  }

}
