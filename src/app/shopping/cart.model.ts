import { Books } from "../books/Books.model";

export class Cart {
    constructor(public book: Books, public quantity: number) { }
}